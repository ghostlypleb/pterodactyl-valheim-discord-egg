module gitlab.com/surdaft/pterodactyl-valheim-discord-egg

go 1.15

require (
	github.com/gorilla/websocket v1.4.2
	github.com/nickname32/discordhook v1.0.2
	github.com/sirupsen/logrus v1.8.1
	github.com/andersfylling/snowflake v1.3.0
)
