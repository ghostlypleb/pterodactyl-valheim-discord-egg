package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"github.com/andersfylling/snowflake"
	"github.com/gorilla/websocket"
	"github.com/nickname32/discordhook"
	log "github.com/sirupsen/logrus"
	"gitlab.com/surdaft/pterodactyl-valheim-discord-egg/pterodactyl"
	"gitlab.com/surdaft/pterodactyl-valheim-discord-egg/pterodactyl/client"
	"gitlab.com/surdaft/pterodactyl-valheim-discord-egg/pterodactyl/objects"
	pterodactylWebsocket "gitlab.com/surdaft/pterodactyl-valheim-discord-egg/pterodactyl/websocket"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var (
	baseUrl      *string = flag.String("apiBaseUri", "", "https://pterodactyl.host.name")
	clientToken  *string = flag.String("apiToken", "", "213")
	webhookID    *uint64 = flag.Uint64("webhookID", uint64(0), "")
	webhookToken *string = flag.String("webhookToken", "", "")
)

var (
	SteamCharacterMap = map[int]string{
		76561198152989653: "Svend",
		76561198060059250: "Olaf Torvald",
		76561198277514748: "Floki",
		76561198162683461: "Elsa Torvald",
	}
)

func main() {
	var (
		err error
	)

	flag.Parse()

	if baseUrl == nil || *baseUrl == "" {
		log.Panic(errors.New("Missing apiBaseUri"))
	}

	if clientToken == nil || *clientToken == "" {
		log.Panic(errors.New("Missing apiToken"))
	}

	if webhookID == nil || *webhookID == uint64(0) {
		log.Panic(errors.New("Missing webhookID"))
	}

	if webhookToken == nil || *webhookToken == "" {
		log.Panic(errors.New("Missing webhookToken"))
	}

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	apiClient := pterodactyl.NewClient(
		*clientToken,
		*baseUrl,
	)

	var servers *client.GetClientsResponse
	servers, err = apiClient.GetClients(nil)
	if err != nil {
		log.Errorf("Error: %s", err.Error())
		os.Exit(0)
	}

	var found *objects.Server
	for _, server := range servers.Data {
		log.Printf("Checking server: %s", server.Attributes.Name)
		if server.Attributes.Name == "Valheim" {
			log.Printf("Found server: %s", server.Attributes.Name)
			found = &server
			break
		}
	}

	if found == nil {
		log.Fatal(errors.New("could not find valheim"))
	}

	websocketConnectionInfo, err := apiClient.GetWebsocketDetails(nil, &client.GetWebsocketDetailsOptions{
		ServerID: found.Attributes.Identifier,
	})

	if err != nil {
		log.Fatal(err)
	}

	websocketHeader := http.Header{}
	websocketHeader.Add("Authorization", "Bearer " + *clientToken)

	// impersonate the panel to connect to the socket
	websocketHeader.Add("Origin", *baseUrl)

	log.WithFields(log.Fields{
		"URL": websocketConnectionInfo.Data.Socket,
	}).Printf("connecting to websocket")

	c, r, err := websocket.DefaultDialer.Dial(websocketConnectionInfo.Data.Socket, websocketHeader)

	if err != nil {
		log.Panic(err)
	}

	bodyRaw, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	log.Printf(string(bodyRaw))

	if err != nil {
		log.Fatal(err)
	}

	sendAuth(c, websocketConnectionInfo.Data.Token)

	defer c.Close()

	done := make(chan struct{})

	go func() {
		wa, _ := discordhook.NewWebhookAPI(snowflake.NewSnowflake(*webhookID), *webhookToken, true, nil)

		var lastPost time.Time

		defer close(done)
		for {
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Println("read:", err)
				return
			}

			var baseEvent pterodactylWebsocket.WebsocketEvent
			json.Unmarshal(message, &baseEvent)

			switch baseEvent.Event {
			case pterodactylWebsocket.WEBSOCKET_EVENT_CONSOLE_OUTPUT:
				// to analyse at a later stage if no matches found
				log.Printf("recv: %s", message)

				var event pterodactylWebsocket.WebsocketEventConsoleOutput
				json.Unmarshal(message, &event)

				var eventMessage string = event.Args[0]

				var message string
				var embeds []*discordhook.Embed

				switch true {
				case strings.Contains(eventMessage, "World saved"):
					regex := regexp.MustCompile(`World saved\s+\(\s+(.+)\s+\)`)
					matches := regex.FindStringSubmatch(eventMessage)

					if len(matches) == 0 {
						log.Printf("unexpected, no matches found")
						break
					}

					timeTaken, _ := time.ParseDuration(matches[1])
					if timeTaken < (time.Second * 5) {
						// its not taking longer than usual
						break
					}

					var fields []*discordhook.EmbedField
					fields = append(fields, &discordhook.EmbedField{
						Name: "**Time Taken**",
						Value: matches[1],
					})

					embeds = append(embeds, &discordhook.Embed{
						Title: "**World Saved**",
						Fields: fields,
					})

					break

				case strings.Contains(eventMessage, "0:0"):
					regex := regexp.MustCompile(`([\w]+)\s:\s0:0`)
					matches := regex.FindStringSubmatch(eventMessage)

					if len(matches) == 0 {
						log.Printf("unexpected, no matches found")
						break
					}

					var fields []*discordhook.EmbedField
					fields = append(fields, &discordhook.EmbedField{
						Name: "**Nickname**",
						Value: matches[1],
					})

					embeds = append(embeds, &discordhook.Embed{
						Title: "**Player Died**",
						Fields: fields,
					})

					break

				case strings.Contains(eventMessage, "Got handshake from client "):
					regex := regexp.MustCompile(`Got handshake from client\s+(\d+)`)
					matches := regex.FindStringSubmatch(eventMessage)

					if len(matches) == 0 {
						log.Printf("unexpected, no matches found")
						break
					}

					steamID, _ := strconv.Atoi(strings.TrimSpace(matches[1]))
					username := "unknown"

					for ID, name := range SteamCharacterMap {
						if steamID == ID {
							username = name
							break
						}
					}

					var fields []*discordhook.EmbedField
					fields = append(fields, &discordhook.EmbedField{
						Name: "**SteamID**",
						Value: strings.TrimSpace(matches[1]),
					})

					fields = append(fields, &discordhook.EmbedField{
						Name: "**Username**",
						Value: username,
					})

					embeds = append(embeds, &discordhook.Embed{
						Title: "**User Connected**",
						Fields: fields,
					})

					break

				case strings.Contains(eventMessage, "Closing socket"):
					regex := regexp.MustCompile(`Closing socket\s+(\d+)`)
					matches := regex.FindStringSubmatch(eventMessage)

					if len(matches) == 0 {
						log.Printf("unexpected, no matches found")
						break
					}

					steamID, _ := strconv.Atoi(strings.TrimSpace(matches[1]))
					username := "unknown"

					for ID, name := range SteamCharacterMap {
						if steamID == ID {
							username = name
							break
						}
					}

					var fields []*discordhook.EmbedField
					fields = append(fields, &discordhook.EmbedField{
						Name: "**SteamID**",
						Value: strings.TrimSpace(matches[1]),
					})

					fields = append(fields, &discordhook.EmbedField{
						Name: "**Username**",
						Value: username,
					})

					embeds = append(embeds, &discordhook.Embed{
						Title: "**User Disconnected**",
						Fields: fields,
					})

					break

				case strings.Contains(eventMessage, "Connections"):
					if true {
						break
					}

					regex := regexp.MustCompile(`Connections\s+(\d+)`)
					matches := regex.FindStringSubmatch(eventMessage)

					if len(matches) == 0 {
						log.Printf("unexpected, no matches found")
						break
					}

					var fields []*discordhook.EmbedField
					fields = append(fields, &discordhook.EmbedField{
						Name: "**Players Online**",
						Value: strings.TrimSpace(matches[1]),
					})

					embeds = append(embeds, &discordhook.Embed{
						Title: "**Server Status**",
						Fields: fields,
					})

					break

				case strings.Contains(eventMessage, "has wrong password"):
					regex := regexp.MustCompile(`\s+(\d+)\s+has wrong password`)
					matches := regex.FindStringSubmatch(eventMessage)

					if len(matches) == 0 {
						log.Printf("unexpected, no matches found")
						break
					}

					steamID, _ := strconv.Atoi(strings.TrimSpace(matches[1]))
					username := "unknown"

					for ID, name := range SteamCharacterMap {
						if steamID == ID {
							username = name
							break
						}
					}

					var fields []*discordhook.EmbedField
					fields = append(fields, &discordhook.EmbedField{
						Name: "**SteamID**",
						Value: strings.TrimSpace(matches[1]),
					})

					fields = append(fields, &discordhook.EmbedField{
						Name: "**Username**",
						Value: username,
					})

					embeds = append(embeds, &discordhook.Embed{
						Title: "**User Entered Incorrect Password**",
						Fields: fields,
					})

					break
				case strings.Contains(eventMessage, "VERSION check"):
					regex := regexp.MustCompile(`their:([^\s]+)\s+mine:([^\s\r\n]+)`)
					matches := regex.FindStringSubmatch(eventMessage)

					if len(matches) == 0 {
						log.Printf("unexpected, no matches found")
						break
					}

					if matches[1] != matches[2] {
						var fields []*discordhook.EmbedField
						fields = append(fields, &discordhook.EmbedField{
							Name: "**User Version**",
							Value: strings.TrimSpace(matches[1]),
						})

						fields = append(fields, &discordhook.EmbedField{
							Name: "**Server Version**",
							Value: strings.TrimSpace(matches[2]),
						})

						embeds = append(embeds, &discordhook.Embed{
							Title: "**User Version Mismatch**",
							Fields: fields,
						})
					}

					break
				}

				if message != "" || len(embeds) > 0 {
					wa.Execute(nil, &discordhook.WebhookExecuteParams{
						Content: message,
						Embeds: embeds,
					}, nil, "")
				}

				break

			case pterodactylWebsocket.WEBSOCKET_EVENT_STATS:
				// disable for now
				if true {
					break
				}

				var event pterodactylWebsocket.WebsocketEventStats
				json.Unmarshal(message, &event)

				for _, rawArg := range event.Args {
					var arg pterodactylWebsocket.WebsocketEventStatsArgs

					json.Unmarshal([]byte(rawArg), &arg)

					event.ParsedArgs = append(event.ParsedArgs, arg)
				}

				// every 30 mins
				now := time.Now()
				if now.Minute() % 30 == 0 && time.Since(lastPost) > (1 * time.Minute) {
					var fields []*discordhook.EmbedField
					fields = append(fields, &discordhook.EmbedField{
						Name: "CPU",
						Value: fmt.Sprintf(
							"%.2f%%",
							event.ParsedArgs[0].CpuAbsolute,
						),
					}, &discordhook.EmbedField{
						Name: "Memory",
						Value: fmt.Sprintf(
							"%.2f%%",
							(float64(event.ParsedArgs[0].MemoryBytes) / float64(event.ParsedArgs[0].MemoryLimitBytes)) * 100,
						),
					})

					var embeds []*discordhook.Embed
					embeds = append(embeds, &discordhook.Embed{
						Fields: fields,
					})

					wa.Execute(nil, &discordhook.WebhookExecuteParams{
						Content: "current system metrics",
						Embeds: embeds,
					}, nil, "")

					lastPost = now
				}

				break

			case pterodactylWebsocket.WEBSOCKET_EVENT_TOKEN_EXPIRING:
				websocketConnectionInfo, err = apiClient.GetWebsocketDetails(nil, &client.GetWebsocketDetailsOptions{
					ServerID: found.Attributes.Identifier,
				})

				if err != nil {
					log.Errorf("Error: %s", err.Error())
					break
				}


				sendAuth(c, websocketConnectionInfo.Data.Token)
				break
			}
		}
	}()


	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-done:
			return
		case <-interrupt:
			log.Println("interrupt")

			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return
		}
	}
}

func sendAuth(c *websocket.Conn, token string) {
	err := c.WriteMessage(websocket.TextMessage,
		[]byte("{\"event\":\"auth\",\"args\":[\"" + token + "\"]}"))

	if err != nil {
		log.Fatal(err)
	}
}