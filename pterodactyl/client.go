package pterodactyl

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/surdaft/pterodactyl-valheim-discord-egg/pterodactyl/client"
	"net/http"
	"time"
)

type Client struct {
	BaseURL string
	apiKey string
	HTTPClient *http.Client
}

func NewClient(apiKey string, baseUrl string) *Client {
	return &Client{
		BaseURL: baseUrl,
		apiKey:  apiKey,
		HTTPClient: &http.Client{
			Timeout: time.Minute,
		},
	}
}

type errorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type successResponse struct {
	Code int         `json:"code"`
	Data interface{} `json:"data"`
}

func (c *Client) sendRequest(req *http.Request, v interface{}) error {
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "Application/vnd.pterodactyl.v1+json")
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.apiKey))

	res, err := c.HTTPClient.Do(req)
	if err != nil {
		return err
	}

	defer res.Body.Close()

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes errorResponse
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return errors.New(errRes.Message)
		}

		return fmt.Errorf("unknown error, status code: %d", res.StatusCode)
	}

	if err = json.NewDecoder(res.Body).Decode(&v); err != nil {
		return err
	}

	return nil
}

func (c *Client) GetClients(ctx *context.Context) (*client.GetClientsResponse, error) {
	var (
		err error
		req *http.Request
		resp client.GetClientsResponse
	)

	req, err = http.NewRequest(
		"GET",
		fmt.Sprintf("%s/api/client", c.BaseURL),
		nil,
	)

	if err != nil {
		return nil, err
	}

	if ctx != nil {
		req.WithContext(*ctx)
	}

	resp = client.GetClientsResponse{}
	if err = c.sendRequest(req, &resp); err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *Client) GetWebsocketDetails(ctx *context.Context, options *client.GetWebsocketDetailsOptions) (*client.GetWebsocketDetailsResponse, error) {
	var (
		err error
		req *http.Request
		resp client.GetWebsocketDetailsResponse
	)

	req, err = http.NewRequest(
		"GET",
		fmt.Sprintf("%s/api/client/servers/%s/websocket", c.BaseURL, options.ServerID),
		nil,
	)

	if err != nil {
		return nil, err
	}

	if ctx != nil {
		req.WithContext(*ctx)
	}

	resp = client.GetWebsocketDetailsResponse{}
	if err = c.sendRequest(req, &resp); err != nil {
		return nil, err
	}

	return &resp, nil
}