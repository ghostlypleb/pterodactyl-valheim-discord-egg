package objects


type Meta struct {
	Pagination Pagination `json:"pagination"`
}

type Pagination struct {
	Total int `json:"total"`
	//count
	Count int `json:"count"`
	// per page
	PerPage int `json:"per_page"`
	// current page
	Current int `json:"current"`
	//total pages
	TotalPages int `json:"total_pages"`
}

type Server struct {
	Attributes ServerAttributes `json:"attributes"`
}

type ServerAttributes struct {
	ServerOwner   bool          `json:"server_owner"`
	Identifier    string        `json:"identifier"`
	UUID          string        `json:"uuid"`
	Name          string        `json:"name"`
	Node          string        `json:"node"`
	SftpDetails   SftpDetails   `json:"sftp_details"`
	Description   string        `json:"description"`
	Limits        Limits        `json:"limits"`
	FeatureLimits FeatureLimits `json:"feature_limits"`
	IsSuspended   bool          `json:"is_suspended"`
	IsInstalling  bool          `json:"is_installing"`
	Relationships Relationships `json:"relationships"`
}

type Relationships struct {
	Allocations Allocations `json:"allocations"`
}

type Allocations struct {
	Data []Allocation `json:"data"`
}

type Allocation struct {
	Attributes AllocationAttributes `json:"attributes"`
}

type AllocationAttributes struct {
	ID int `json:"id"`
	IP string `json:"ip"`
	IPAlias string `json:"ip_alias"`
	Port int `json:"port"`
	Notes string `json:"notes"`
	IsDefault bool `json:"is_default"`
}

type FeatureLimits struct {
	Databases int `json:"databases"`
	Allocations int `json:"allocation"`
	Backups int `json:"backups"`
}

type Limits struct {
	Memory int `json:"memory"`
	Swap int `json:"swap"`
	Disk int `json:"disk"`
	IO int `json:"io"`
	CPU int `json:"cpu"`
}

type SftpDetails struct {
	IP string `json:"ip"`
	Port int `json:"port"`
}

type ServerWebsocket struct {
	Token string `json:"token"`
	Socket string `json:"socket"`
}