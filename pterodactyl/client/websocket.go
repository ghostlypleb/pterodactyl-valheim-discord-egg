package client

import (
	"gitlab.com/surdaft/pterodactyl-valheim-discord-egg/pterodactyl/objects"
)

type GetWebsocketDetailsResponse struct {
	Data objects.ServerWebsocket `json:"data"`
}

type GetWebsocketDetailsOptions struct {
	ServerID string
}