package client

import (
	"gitlab.com/surdaft/pterodactyl-valheim-discord-egg/pterodactyl/objects"
)

type GetClientsResponse struct {
	Data []objects.Server `json:"data"`
	Meta objects.Meta     `json:"meta"`
}