package websocket

// auth success
const WEBSOCKET_EVENT_AUTH_SUCCESS = "auth success"
// status
const WEBSOCKET_EVENT_STATUS = "status"
// stats
const WEBSOCKET_EVENT_STATS = "stats"
// token expiring
const WEBSOCKET_EVENT_TOKEN_EXPIRING = "token expiring"
// console output
const WEBSOCKET_EVENT_CONSOLE_OUTPUT = "console output"

type WebsocketEvent struct {
	Event string `json:"event"`
	Args []string `json:"args"`
}

type WebsocketEventAuthSuccess struct {
	Event string `json:"event"`
}

type WebsocketEventStatus struct {
	Event string `json:"event"`
	Args []string `json:"args"`
}

type WebsocketEventConsoleOutput struct {
	Event string `json:"event"`
	Args []string `json:"args"`
}

type WebsocketEventStats struct {
	Event string `json:"event"`
	Args []string `json:"args"`
	ParsedArgs []WebsocketEventStatsArgs
}

type WebsocketEventStatsArgs struct {
	MemoryBytes      int                        `json:"memory_bytes"`
	MemoryLimitBytes int                        `json:"memory_limit_bytes"`
	CpuAbsolute      float64                    `json:"cpu_absolute"`
	Network          WebsocketEventStatsNetwork `json:"network"`
	State            string                     `json:"state"`
	DiskBytes        int                        `json:"disk_bytes"`
}

type WebsocketEventStatsNetwork struct {
	RxBytes int `json:"rx_bytes"`
	TxBytes int `json:"tx_bytes"`
}
