build:
	bash -x scripts/build.sh

upload:
	scp pterodactylBot_linux-amd64 hetzner:/opt/monitoring/pterodactylBot
	ssh hetzner 'cd /opt/monitoring && docker-compose stop pterodactyl-bot && docker-compose up --build -d pterodactyl-bot'
