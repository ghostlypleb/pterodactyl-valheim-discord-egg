env GOARCH=amd64 GOOS=linux go build -o pterodactylBot_linux-amd64 main.go
env GOARCH=amd64 GOOS=darwin go build -o pterodactylBot_darwin-amd64 main.go
env GOARCH=amd64 GOOS=windows go build -o pterodactylBot_win-amd64.exe main.go